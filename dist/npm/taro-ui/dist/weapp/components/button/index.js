"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _index = require("../../../../../@tarojs/taro-weapp/index.js");

var _index2 = _interopRequireDefault(_index);

var _index3 = require("../../../../../prop-types/index.js");

var _index4 = _interopRequireDefault(_index3);

var _component = require("../../common/component.js");

var _component2 = _interopRequireDefault(_component);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SIZE_CLASS = {
  normal: 'normal',
  small: 'small'
};

var TYPE_CLASS = {
  primary: 'primary',
  secondary: 'secondary'
};

var AtButton = function (_AtComponent) {
  _inherits(AtButton, _AtComponent);

  function AtButton() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, AtButton);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AtButton.__proto__ || Object.getPrototypeOf(AtButton)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["$compid__5", "loading", "rootClassName", "disabled", "size", "type", "circle", "children"], _this.customComponents = ["AtLoading"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(AtButton, [{
    key: "_constructor",
    value: function _constructor() {
      _get(AtButton.prototype.__proto__ || Object.getPrototypeOf(AtButton.prototype), "_constructor", this).apply(this, arguments);
      this.state = {};
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: "onClick",
    value: function onClick() {
      if (!this.props.disabled) {
        var _props;

        this.props.onClick && (_props = this.props).onClick.apply(_props, arguments);
      }
    }
  }, {
    key: "_createData",
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__5"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__5 = _genCompid2[0],
          $compid__5 = _genCompid2[1];

      var _props2 = this.__props,
          _props2$size = _props2.size,
          size = _props2$size === undefined ? 'normal' : _props2$size,
          _props2$type = _props2.type,
          type = _props2$type === undefined ? '' : _props2$type,
          circle = _props2.circle,
          loading = _props2.loading,
          disabled = _props2.disabled;

      var rootClassName = ['at-button'];
      var sizeClass = SIZE_CLASS[size] || '';
      var disabledClass = disabled ? 'at-button--disabled' : '';
      var typeClass = TYPE_CLASS[type] ? "at-button--" + type : '';
      var circleClass = circle ? 'at-button--circle' : '';

      rootClassName.push("at-button--" + sizeClass, typeClass, circleClass, disabledClass);
      rootClassName = rootClassName.filter(function (str) {
        return str !== '';
      });
      var loadingColor = type === 'primary' ? '#fff' : '#6190E8';
      var loadingSize = size === 'small' ? '16' : '18';
      var component = void 0;
      if (loading) {
        rootClassName.push('at-button--icon');
        _index.propsManager.set({
          "color": loadingColor,
          "size": loadingSize
        }, $compid__5, $prevCompid__5);
      }

      Object.assign(this.__state, {
        $compid__5: $compid__5,
        loading: loading,
        rootClassName: rootClassName
      });
      return this.__state;
    }
  }]);

  return AtButton;
}(_component2.default);

AtButton.$$events = ["onClick"];
AtButton.$$componentPath = "Users/leon/Desktop/poster/node_modules/taro-ui/dist/weapp/components/button/index";


AtButton.defaultProps = {
  size: 'normal',
  type: '',
  circle: false,
  loading: false,
  disabled: false,
  onClick: function onClick() {}
};

AtButton.propTypes = {
  size: _index4.default.oneOf(['normal', 'small']),
  type: _index4.default.oneOf(['primary', 'secondary']),
  circle: _index4.default.bool,
  loading: _index4.default.bool,
  disabled: _index4.default.bool,
  onClick: _index4.default.func
};
exports.default = AtButton;

Component(require('../../../../../@tarojs/taro-weapp/index.js').default.createComponent(AtButton));