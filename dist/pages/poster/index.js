'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _index = require('../../npm/@tarojs/taro-weapp/index.js');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//import TaroCanvasDrawer from '../../component/taro-plugin-canvas/index'; // 拷贝文件到component的引入方式
// npm 引入方式


var posterConfig = {
  jdConfig: {
    width: 750,
    height: 1334,
    backgroundColor: '#fff',
    debug: false,
    pixelRatio: 1,
    blocks: [{
      width: 690,
      height: 808,
      x: 30,
      y: 183,
      borderWidth: 2,
      borderColor: '#f0c2a0',
      borderRadius: 20
    }, {
      width: 634,
      height: 74,
      x: 59,
      y: 770,
      backgroundColor: '#fff',
      opacity: 0.5,
      zIndex: 100
    }],
    texts: [{
      x: 113,
      y: 61,
      baseLine: 'middle',
      text: '伟仔',
      fontSize: 32,
      color: '#8d8d8d'
    }, {
      x: 30,
      y: 113,
      baseLine: 'top',
      text: '发现一个好物，推荐给你呀',
      fontSize: 38,
      color: '#080808'
    }, {
      x: 92,
      y: 810,
      fontSize: 38,
      baseLine: 'middle',
      text: '标题标题标题标题标题标题标题标题标题',
      width: 570,
      lineNum: 1,
      color: '#8d8d8d',
      zIndex: 200
    }, {
      x: 59,
      y: 895,
      baseLine: 'middle',
      text: [{
        text: '2人拼',
        fontSize: 28,
        color: '#ec1731'
      }, {
        text: '¥99',
        fontSize: 36,
        color: '#ec1731',
        marginLeft: 30
      }]
    }, {
      x: 522,
      y: 895,
      baseLine: 'middle',
      text: '已拼2件',
      fontSize: 28,
      color: '#929292'
    }, {
      x: 59,
      y: 945,
      baseLine: 'middle',
      text: [{
        text: '商家发货&售后',
        fontSize: 28,
        color: '#929292'
      }, {
        text: '七天退货',
        fontSize: 28,
        color: '#929292',
        marginLeft: 50
      }, {
        text: '运费险',
        fontSize: 28,
        color: '#929292',
        marginLeft: 50
      }]
    }, {
      x: 360,
      y: 1065,
      baseLine: 'top',
      text: '长按识别小程序码',
      fontSize: 38,
      color: '#080808'
    }, {
      x: 360,
      y: 1123,
      baseLine: 'top',
      text: '超值好货一起拼',
      fontSize: 28,
      color: '#929292'
    }],
    images: [{
      width: 62,
      height: 62,
      x: 30,
      y: 30,
      borderRadius: 62,
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/02bb99132352b5b5dcea.jpg'
    }, {
      width: 634,
      height: 634,
      x: 59,
      y: 210,
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/193256f45999757701f2.jpeg'
    }, {
      width: 220,
      height: 220,
      x: 92,
      y: 1020,
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/d719fdb289c955627735.jpg'
    }, {
      width: 750,
      height: 90,
      x: 0,
      y: 1244,
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/67b0a8ad316b44841c69.png'
    }]

  },
  demoConfig: {
    width: 750,
    height: 1000,
    backgroundColor: '#fff',
    debug: false,
    blocks: [{
      x: 0,
      y: 10,
      width: 750, // 如果内部有文字，由文字宽度和内边距决定
      height: 120,
      paddingLeft: 0,
      paddingRight: 0,
      borderWidth: 10,
      borderColor: 'red',
      backgroundColor: 'blue',
      borderRadius: 40,
      text: {
        text: [{
          text: '金额¥ 1.00',
          fontSize: 80,
          color: 'yellow',
          opacity: 1,
          marginLeft: 50,
          marginRight: 10
        }, {
          text: '金额¥ 1.00',
          fontSize: 20,
          color: 'yellow',
          opacity: 1,
          marginLeft: 10,
          textDecoration: 'line-through'
        }],
        baseLine: 'middle'
      }
    }],
    texts: [{
      x: 0,
      y: 180,
      text: [{
        text: '长标题长标题长标题长标题长标题长标题长标题长标题长标题',
        fontSize: 40,
        color: 'red',
        opacity: 1,
        marginLeft: 0,
        marginRight: 10,
        width: 200,
        lineHeight: 40,
        lineNum: 2
      }, {
        text: '原价¥ 1.00',
        fontSize: 40,
        color: 'blue',
        opacity: 1,
        marginLeft: 10,
        textDecoration: 'line-through'
      }],
      baseLine: 'middle'
    }, {
      x: 10,
      y: 330,
      text: '金额¥ 1.00',
      fontSize: 80,
      color: 'blue',
      opacity: 1,
      baseLine: 'middle',
      textDecoration: 'line-through'
    }],
    images: [{
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/02bb99132352b5b5dcea.jpg',
      width: 300,
      height: 300,
      y: 450,
      x: 0
      // borderRadius: 150,
      // borderWidth: 10,
      // borderColor: 'red',
    }, {
      url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/02bb99132352b5b5dcea.jpg',
      width: 100,
      height: 100,
      y: 450,
      x: 400,
      borderRadius: 100,
      borderWidth: 10
    }],
    lines: [{
      startY: 800,
      startX: 10,
      endX: 300,
      endY: 800,
      width: 5,
      color: 'red'
    }]

  }
};

var Simple = function (_BaseComponent) {
  _inherits(Simple, _BaseComponent);

  function Simple() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Simple);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Simple.__proto__ || Object.getPrototypeOf(Simple)).call.apply(_ref, [this].concat(args))), _this), _this.$usedState = ["$compid__4", "title", "config", "shareImage", "canvasStatus", "demoConfig", "rssConfig"], _this.config = {
      navigationBarTitleText: '首页'
    }, _this.canvasDrawFunc = function () {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _this.state.rssConfig;

      _this.setState({
        canvasStatus: true,
        config: config
      });
      _index2.default.showLoading({
        title: '绘制中...'
      });
    }, _this.onCreateSuccess = function (result) {
      console.log(result);
      var tempFilePath = result.tempFilePath,
          errMsg = result.errMsg;

      _index2.default.hideLoading();
      if (errMsg === 'canvasToTempFilePath:ok') {
        _index2.default.getImageInfo({
          src: tempFilePath,
          success: function success(res) {
            console.log(res);
          }
        });
        _this.setState({
          shareImage: tempFilePath,
          title: '绘制后',
          // 重置 TaroCanvasDrawer 状态，方便下一次调用
          canvasStatus: false,
          config: null
        });
      } else {
        // 重置 TaroCanvasDrawer 状态，方便下一次调用
        _this.setState({
          canvasStatus: false,
          config: null
        });
        _index2.default.showToast({ icon: 'none', title: errMsg || '出现错误' });
        console.log(errMsg);
      }
      // 预览
      // Taro.previewImage({
      //   current: tempFilePath,
      //   urls: [tempFilePath]
      // })
    }, _this.onCreateFail = function (error) {
      _index2.default.hideLoading();
      // 重置 TaroCanvasDrawer 状态，方便下一次调用
      _this.setState({
        canvasStatus: false,
        config: null
      });
      console.log(error);
    }, _this.saveToAlbum = function () {
      var res = _index2.default.saveImageToPhotosAlbum({
        filePath: _this.state.shareImage
      });
      if (res.errMsg === 'saveImageToPhotosAlbum:ok') {
        _index2.default.showToast({
          title: '保存图片成功',
          icon: 'success',
          duration: 2000
        });
      }
    }, _this.customComponents = ["TaroCanvasDrawer"], _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Simple, [{
    key: '_constructor',
    value: function _constructor(props) {
      _get(Simple.prototype.__proto__ || Object.getPrototypeOf(Simple.prototype), '_constructor', this).call(this, props);
      this.state = {
        title: '绘制前',
        // 绘图配置文件
        config: null,
        // 绘制的图片
        shareImage: null,
        // TaroCanvasDrawer 组件状态
        canvasStatus: false,
        demoConfig: posterConfig.jdConfig,
        rssConfig: {
          width: 750,
          height: 750,
          backgroundColor: '#fff',
          debug: false,
          blocks: [{
            x: 0,
            y: 0,
            width: 750,
            height: 750,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            // borderColor: '#ccc',
            backgroundColor: '#EFF3F5',
            borderRadius: 0
          }, {
            x: 40,
            y: 40,
            width: 670,
            height: 670,
            paddingLeft: 0,
            paddingRight: 0,
            borderWidth: 0,
            // borderColor: '#ccc',
            backgroundColor: '#fff',
            borderRadius: 12
          }],
          texts: [{
            x: 80,
            y: 420,
            text: '国产谍战 真人演出,《隐形守护者》凭什么成为Steam第一?',
            fontSize: 32,
            color: '#000',
            opacity: 1,
            baseLine: 'middle',
            lineHeight: 48,
            lineNum: 2,
            textAlign: 'left',
            width: 580,
            zIndex: 999
          }, {
            x: 80,
            y: 590,
            text: '长按扫描二维码阅读完整内容',
            fontSize: 24,
            color: '#666',
            opacity: 1,
            baseLine: 'middle',
            textAlign: 'left',
            lineHeight: 36,
            lineNum: 1,
            zIndex: 999
          }, {
            x: 80,
            y: 640,
            text: '分享来自 「 RssFeed 」',
            fontSize: 24,
            color: '#666',
            opacity: 1,
            baseLine: 'middle',
            textAlign: 'left',
            lineHeight: 36,
            lineNum: 1,
            zIndex: 999
          }],
          images: [{
            url: 'http://pic.juncao.cc/rssfeed/images/demo.png',
            width: 670,
            height: 320,
            y: 40,
            x: 40,
            borderRadius: 12,
            zIndex: 10
            // borderRadius: 150,
            // borderWidth: 10,
            // borderColor: 'red',
          }, {
            url: 'https://pic.juncao.cc/cms/images/minapp.jpg',
            width: 110,
            height: 110,
            y: 570,
            x: 560,
            borderRadius: 100,
            borderWidth: 0,
            zIndex: 10
          }],
          lines: [{
            startY: 540,
            startX: 80,
            endX: 670,
            endY: 541,
            width: 1,
            color: '#eee'
          }]
        }
      };
      this.$$refs = new _index2.default.RefsArray();
    }
  }, {
    key: 'onShareAppMessage',
    value: function onShareAppMessage() {
      var _state = this.state,
          title = _state.title,
          path = _state.path,
          shareImage = _state.shareImage;

      return {
        title: title,
        path: '/page/user?id=123',
        imageUrl: shareImage
      };
    }

    // 调用绘画 => canvasStatus 置为true、同时设置config


    // 绘制成功回调函数 （必须实现）=> 接收绘制结果、重置 TaroCanvasDrawer 状态


    // 绘制失败回调函数 （必须实现）=> 接收绘制错误信息、重置 TaroCanvasDrawer 状态


    // 保存图片至本地

  }, {
    key: '_createData',
    value: function _createData() {
      this.__state = arguments[0] || this.state || {};
      this.__props = arguments[1] || this.props || {};
      var __isRunloopRef = arguments[2];
      var __prefix = this.$prefix;
      ;

      var _genCompid = (0, _index.genCompid)(__prefix + "$compid__4"),
          _genCompid2 = _slicedToArray(_genCompid, 2),
          $prevCompid__4 = _genCompid2[0],
          $compid__4 = _genCompid2[1];

      this.__state.canvasStatus && _index.propsManager.set({
        "config": this.__state.demoConfig,
        "onCreateSuccess": this.onCreateSuccess,
        "onCreateFail": this.onCreateFail
      }, $compid__4, $prevCompid__4);
      Object.assign(this.__state, {
        $compid__4: $compid__4
      });
      return this.__state;
    }
  }]);

  return Simple;
}(_index.Component);

Simple.$$events = ["canvasDrawFunc", "saveToAlbum"];
Simple.$$componentPath = "pages/poster/index";
exports.default = Simple;

Component(require('../../npm/@tarojs/taro-weapp/index.js').default.createComponent(Simple, true));